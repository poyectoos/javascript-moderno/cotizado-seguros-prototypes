

/*
==================================================
          Objeto para el seguro o la poliza
==================================================
*/

function Seguro(marca, anno, tipo) {
  this.marca = parseInt(marca);
  this.anno = anno;
  this.tipo = tipo;
}
Seguro.prototype.cotizar = function() {
  /*
    1 = 1.15
    2 = 1.05
    3 = 1.35
  */
 let cantidad;
 const base = 2000;
  switch (this.marca) {
    case 1:
      cantidad = base*1.15;
      break;
    case 2:
      cantidad = base*1.05;
      break;
    case 3:
      cantidad = base*1.35;
      break;
  
    default:
      cantidad = 0;
      break;
  }
  // EL precio baja 3% cada año
  const diferencia = new Date().getFullYear()-this.anno;
  cantidad -= (cantidad*(diferencia*0.03));
  /*
    Si el seguro es basico se multiplica por 30%
    Si el seguro es completo se multiplica por 50%
  */
  if (this.tipo === 'basico') {
    cantidad = cantidad*1.3;
  } else if (this.tipo === 'completo') {
    cantidad = cantidad*1.5;
  }

  return cantidad;
}
/*
==================================================
          Objeto para el formulario
==================================================
*/

  function UI() {}

  UI.prototype.llenarOpciones = () => {
    const max = new Date().getFullYear();
    const min = max - 20;

    const selectorYear = document.querySelector('#year');

    for (let i = max; i > min ; i--) {
      let option = document.createElement('option');
      option.value = i;
      option.textContent = i;
      selectorYear.appendChild(option);
    }
  }
  UI.prototype.mensaje = (texto, tipo) => {
    const div = document.createElement('div');
    if (tipo === 'error') {
      div.classList.add('error');
    } else {
      div.classList.add('correcto');
    }
    div.classList.add('mensaje', 'mt-5');
    div.textContent = texto;

    const formulario = document.querySelector('#cotizar-seguro');

    formulario.insertBefore(div, document.querySelector('#resultado'));

    setTimeout(() => {
      div.remove();
    }, 3000);
  }
  UI.prototype.resultado = (seguro, cantidad) => {


    let marca;

    if (seguro.marca === 1) {
      marca = 'Americano';
    } else if (seguro.marca === 2) {
      marca = 'Asiatico';
    } else if (seguro.marca === 3) {
      marca = 'Europeo';
    }
    const div = document.createElement('div');
    div.classList.add('mt-5');
    div.innerHTML = `
    <p class="header">Tu resumen</p>
    <p><span class="font-bold">Marca: </span>${marca}</p>
    <p><span class="font-bold">Año: </span>${seguro.anno}</p>
    <p class="capitalize"><span class="font-bold">Tipo de seguro: </span>${seguro.tipo}</p>
    
    <p><span class="font-bold">Total: </span>$${cantidad}</p>
    `;
    
    //Borramos cotizaciones previas si existen
    const cotizacionPrevia = document.querySelector('#resultado div');
    if (cotizacionPrevia !== null) {
      cotizacionPrevia.remove();
    }

    const resultado = document.querySelector('#resultado');

    // Mostramos spinner
    const spinner = document.querySelector('#cargando');
    spinner.style.display = 'block';
    
    setTimeout(() => {
      spinner.style.display = 'none';
      resultado.appendChild(div);
    }, 3000);
  }

/*
==================================================
          Aplicacion
==================================================
*/

const ui = new UI();

document.addEventListener('DOMContentLoaded', main);

function main() {
  // llenamos el select con los ños
  ui.llenarOpciones();
}

eventListeners();
function eventListeners() {
  const formulario = document.querySelector('#cotizar-seguro');
  formulario.addEventListener('submit', cotizarSeguro);
}

function cotizarSeguro(e) {
  e.preventDefault();

  const marca = document.querySelector('#marca');
  const anno = document.querySelector('#year');
  const tipo = document.querySelector('input[name="tipo"]:checked');

  if (marca.value.trim() === '' || anno.value.trim() === '' || tipo.value.trim() === '') {
    ui.mensaje('Todos los campos son obligatorios', 'error');
    return;
  }
  
  ui.mensaje('cotizando', 'exito');

  // Se insancia el seguro
  const seguro = new Seguro(marca.value.trim(), anno.value.trim(), tipo.value);
  const cantidad = seguro.cotizar();

  ui.resultado(seguro, cantidad);
}

